import { ChatService } from './../../services/chat/chat.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.css']
})
export class ChatbotComponent {
  messages: any[] = [];
  userMessage: string = '';

  constructor (private chatService: ChatService) {}

  ngOnInit() {
    this.messages.push({ role: 'received', content: 'Olá! Como posso ajudar?' });
  }

  sendMessage() {
    const userMessage = this.userMessage;
    this.messages.push({ role: 'sent', content: userMessage });
    this.userMessage = '';

    this.chatService.fazerRequisicao(userMessage).subscribe(
      (response: any) => {

        const chatResponse = response.choices[0].text;
        this.messages.push({ role: 'received', content: chatResponse });

      },
      (errorCode: number) => {
        if (errorCode === 429) {
          const chatResponse = 'Limite de taxa excedido. Tente novamente mais tarde.';
          this.messages.push({ role: 'received', content: chatResponse });
        } else {  
          const chatResponse = 'Ocorreu um erro desconhecido. Tente novamente mais tarde.';
          this.messages.push({ role: 'received', content: chatResponse });
        }
      }
      
      );
  }

}