import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private apiUrl = "https://api.openai.com/v1/chat/completions";
  private apiKey = "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";
  private id_modelo = "gpt-3.5-turbo";
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${this.apiKey}`
  });
  
  constructor(private http: HttpClient) { }
  
  public fazerRequisicao(prompt: string): Observable<any> {

    const body = {
      "model": this.id_modelo,
      "messages": [{ "role": "user", "content": `${prompt}` }]
    }

    return this.http.post(this.apiUrl, body, { headers: this.headers } ).pipe(
      catchError((error: HttpErrorResponse) => {

        const errorCode = error.status;

        return throwError(errorCode);
      })
    );
  }

}
