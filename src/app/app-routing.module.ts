import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicialComponent } from './pages/inicial/inicial.component';
import { ChatbotComponent } from './pages/chatbot/chatbot.component';

const routes: Routes = [
  { path: "", component: InicialComponent },
  { path: "chatbot", component: ChatbotComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
